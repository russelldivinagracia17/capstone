import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import {Link, Navigate, useNavigate} from 'react-router-dom'
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext.js';


export default function AdminDashBoard(){


	const {user, setUser} = useContext(UserContext);
	const navigate = useNavigate();



	return(
		
		<Container>
			<Row>
				<h1 className= 'text-center mt-5'>Welcome Admin</h1>
				<Col className= 'col-12 col-md-6 mt-5'>
					<Card className='bg-warning'>
				      <Card.Body>
				        <Card.Title>Create Product</Card.Title>
				        <Card.Text>
				          Create Products to add in store.
				        </Card.Text>
				        <Button variant="primary" as = {Link} to = '/createProducts'>Create</Button>
				      </Card.Body>
				    </Card>
				</Col>
				<Col className= 'col-12 col-md-6 mt-5'>
				    <Card className='bg-primary'>
				      <Card.Body>
				        <Card.Title>Retrieve all Product</Card.Title>
				        <Card.Text>
				          View all Products in the store.
				        </Card.Text>
				        <Button variant="warning" as = {Link} to = '/products'>Retrieve</Button>
				      </Card.Body>
				    </Card>
				</Col>
			</Row>
		</Container>
	)
}