import Cartcard from '../components/Cartcard.js'
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useContext, useEffect} from 'react';
import {useNavigate, Navigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';

export default function ViewCart(){

	const {user} =useContext(UserContext)
	const [userId, setUserId] =useState('')
	const [products, setProducts] =useState('')
	const [totalAmount, setTotalAmount] =useState('')
	const navigate = useNavigate();
	const token = localStorage.getItem('token');

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/carts/cartProduct`, {
			method: 'GET',
			headers: {
				authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setTotalAmount(data.totalAmount);
			setUserId(data.userId);
			setProducts(data.products.map(product => {
				return(
					<Cartcard key = {product.productId} productProp = {product}/>
				)
			}))
		})
	}, [])

	function checkOut(){

		Swal2.fire({
		  position: 'top-end',
		  icon: 'success',
		  title: 'Successful purchased',
		  showConfirmButton: false,
		  timer: 1500
		})
		.then(()=>{
			navigate('/')
		})
	}


	return(
				
		<Container>
			<Row>
				<Col className='text-bg-primary'>
					<Card className="m-5">
					    <Card.Body className='bg-warning'>
					        <Card.Title className='fs-1 fst-bold'>My Cart</Card.Title>
					        <Card.Text className='fs-3 fst-italic'>Total Amount </Card.Text>
					        <Card.Text>{totalAmount} </Card.Text>
					        <Button variant="primary" onClick={checkOut}>Check out</Button>
					    </Card.Body>
					</Card>
				</Col>
			</Row>
			<Row>
				<Col className='text-bg-dark m-3'>
					{products}
				</Col>
			</Row>
		</Container>
	)
}