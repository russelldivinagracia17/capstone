import {Container, Row, Col, Card, ListGroup, Button, Form} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {useParams, useNavigate} from 'react-router-dom';
import Swal2 from 'sweetalert2'

export default function ViewProduct(){

	const[name, setName] = useState('');
	const[description, setDescription] = useState('');
	const[price, setPrice] =useState('');
	const[stocks, setStocks] =useState('')
	const[quantity, setQuantity] =useState('')
	const[isDisabled, setIsDisabled] =useState(true)
	const {productId} = useParams(); 
	const token = localStorage.getItem('token');
	const navigate = useNavigate();

	

	function viewProduct() {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/view`,{
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			},
		})
		.then(result => result.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks)
		})
	}

	function addProduct(event){

		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/carts/addToCart`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				 authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data){
				Swal2.fire({
					title: 'Successful',
					icon: 'success',
					text: 'added to cart!'
				})
			} else {
				Swal2.fire({
					title: 'Error',
					icon: 'error',
					text: 'Please try again'
				})
			}

		})
		.then(() => {
			navigate('/store')
		})
	}

	useEffect(() => {
		viewProduct();
		if(quantity !== ''){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
		
	}, [quantity]);

	function nav(){
		navigate('/store')
	}

	const quantityValue = (value) => {

		if(value < 0){
			setQuantity(0);
		} else {
			setQuantity(value)
		}
	}

	return(
		<Container className='m-5 p-5 ms-auto'>
			<Row>
				<Col className='col-6'>
					<Form>
						<Card>
						   
						   <Card.Body>
						     <Card.Title>{name}</Card.Title>
						     <Card.Text>
						       {description}
						     </Card.Text>
						     <Card.Text>
						       Price: Php {price}
						     </Card.Text>
						   </Card.Body>
						   <ListGroup className="list-group-flush">					     
							     <ListGroup.Item>Available Stocks: {stocks}</ListGroup.Item>
							     <ListGroup.Item>This item is available in our store and will be delivered within 5 working days.</ListGroup.Item>
						   </ListGroup>
						   <Card.Body>
						   <Row>
							   	<Col>
							   		<input className='col-4' type="number" value={quantity} onChange= {(event) => quantityValue(Number(event.target.value))} placeholder="  Quantity"/>							     	
							   	</Col>
						   </Row>
						   <br/>
						   <Row>
							   <Col>						  
							    	<Button variant="primary" onClick={addProduct} disabled = {isDisabled}>Add to Cart</Button>
							   </Col>
							   <Col>						  
							    	<Button variant="primary" onClick={nav}>Cancel</Button>
							   </Col>
						   </Row>						  
						   </Card.Body>
						 </Card>
					</Form>
				</Col>
			</Row>
		</Container>

		)
}
