import {Container, Row, Col, Button, Form, InputGroup} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {useParams, useNavigate} from 'react-router-dom';
import Swal2 from 'sweetalert2';
import UserContext from '../UserContext.js';


export default function UpdateProduct(){

	const {user} = useContext(UserContext);
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [stocks, setStocks] = useState('');
	const [isActive, setIsActive] = useState('');
	const {productId} = useParams();
	const navigate = useNavigate();
	const token = localStorage.getItem('token');



	function updateProduct(event){

		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`,{
			method: 'PATCH',
			headers:{
				'Authorization': `Bearer ${token}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				stocks: stocks,
				isActive: isActive
			})
		})
		.then(result => result.json())
		.then(data => {
			if(!data){
				Swal2.fire({
					title: 'Unsuccessful',
					icon: 'error',
					text: 'Please try again'
				})
			} else {

				Swal2.fire({
					title: 'Successful',
					icon: 'success',
					text: 'Product added!'
				})
				.then(()=> {
				navigate('/products');
					
				})
			}
		})
	}

	useEffect(() => {
		getProduct();

	}, []);

	function getProduct (){

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`,{
			method: 'GET',
			headers: {
				'Authorization': `Bearer ${token}`
			},
		})
		.then(result => result.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks)
		})
	}

	

	


	return(
		
		<Container>
			<Row>
			<h1 className= 'm-5 text-center'>Update Product</h1>
				<Col>
					
					<Form onSubmit = {event => updateProduct(event)}>
					    <Form.Group className="mb-3" controlId="formProductName">
					      	<Form.Label>Name of Product</Form.Label>
					      	<Form.Control type="string" value = {name} onChange = {event => setName(event.target.value)} placeholder="Product Name" />
					    </Form.Group>
					    <Form.Group className="mb-3" controlId="formProductDescription">
					      	<Form.Label>Description</Form.Label>
					      	<Form.Control type="String" value = {description} onChange = {event => setDescription(event.target.value)} placeholder="Product Description" />
					    </Form.Group>
				        <Row>
				          	<Col>
				          		<Form.Group className="mb-3" controlId="formProductPrice">
					          		<Form.Label>Price</Form.Label>
					            	<Form.Control type="number" value = {price} onChange = {event => setPrice(event.target.value)} placeholder="Price" />
				            	</Form.Group>
				          	</Col>
				          	<Col>
				          		<Form.Group className="mb-3" controlId="formProductStock">
					          		<Form.Label>Stocks</Form.Label>
					            	<Form.Control type="number" value = {stocks} onChange = {event => setStocks(event.target.value)} placeholder="Stocks" />
				            	</Form.Group>
				          	</Col>
				        </Row>
			                <div key={`archived`} className="mb-3">
			                  <Form.Check // prettier-ignore
			                    type={'checkbox'}
			                    checked={isActive}
			                    onChange = {event => setIsActive(event.target.checked)}
			                    id={'archived'}
			                    label={'isActive'}
			                  />
			                </div>
						<Button className='mt-3' variant="success" type="submit">Submit</Button>
					</Form>
				</Col>
			</Row>
		</Container>
		
		)
}