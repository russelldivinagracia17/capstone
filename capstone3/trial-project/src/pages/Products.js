import Productcard from '../components/Productcard.js'
import {useState, useContext, useEffect} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext.js';



export default function Products(){

	const {user} =useContext(UserContext)
	const [products, setProducts] =useState('')
	const navigate = useNavigate();
	const token = localStorage.getItem('token');

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/findAll`, {
			method: 'GET',
			headers: {
				authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setProducts(data.map(product => {
				return(
					<Productcard key = {product._id} productProp = {product}/>
				)
			}))
		})
	}, [])
	

	return(
		
		<>
		<h1 className = 'text-center mt-3'>Products</h1>
		{products}
		</>
		
		
		)
}