import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom';




export default function Storecard(props){

	const{user} = useContext(UserContext)

	const {_id, name, description, price, stocks, isActive, createdOn} = props.storeProp;


	return(
			<>
			<Container className='bg-dark'>
				<Row  className ='col-12'>
					<Col className ="m-3">
						<Card className="text-bg-light"> 
		                      <Card.Body>
		                        <Card.Title>{name}</Card.Title>
		                        
		                        <Card.Subtitle>Description:</Card.Subtitle>
		                        <Card.Text>{description}</Card.Text>

		                        <Card.Subtitle>Price:</Card.Subtitle>
		                        <Card.Text>Php {price}</Card.Text>

		                        <Card.Subtitle>Stocks</Card.Subtitle>
		                        <Card.Text>{stocks}</Card.Text>

		                        <Card.Subtitle>Available</Card.Subtitle>
		                        <Card.Text>{isActive}</Card.Text>

		                        <Card.Subtitle>Date Realeased</Card.Subtitle>
		                        <Card.Text>{createdOn}</Card.Text>

		                        {
		                        user.id === null
		                        ?
		                        <>
		                        <Row>
		                        	<Col>
		                        		<Button as = {Link} to = {`/Login`}>login</Button>
		                        	</Col>
		                        </Row>
		                        </>

		                        :
		                        
		                        <>
		                        <Row>
		                        	<Col>
		                        		<Button as = {Link} to = {`/products/${_id}/cart`}>Purchase</Button>
		                        	</Col>
		                        </Row>
		                        </>
		                        }

		                      </Card.Body>
		                </Card>
					</Col>
				</Row>
			</Container>
            </>
		)
}