import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import {useContext} from 'react';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2'


export default function Highlights(){

	const {user} = useContext(UserContext);

	function playStation(){

		Swal2.fire({
		  title: 'PlayStation',
		  text: 'PlayStation is a video gaming brand that consists of five home video game consoles, two handhelds, a media center, and a smartphone, as well as an online service and multiple magazines. The brand is produced by Sony Interactive Entertainment, a division of Sony; the first PlayStation console was released in Japan in December 1994, and worldwide the following year.',
		  imageUrl: 'https://wallpapercave.com/wp/wp2576751.jpg',
		  imageWidth: 400,
		  imageHeight: 200,
		  imageAlt: 'Custom image',
		})
	}

	function nintendoSwitch(){

		Swal2.fire({
		  title: 'Nintendo Switch',
		  text: 'Nintendo is a Japanese multinational video game company headquartered in Kyoto. It develops, publishes and releases both video games and video game consoles.',
		  imageUrl: 'https://www.underconsideration.com/brandnew/archives/nintendo_switch_logo.png',
		  imageWidth: 400,
		  imageHeight: 200,
		  imageAlt: 'Custom image',
		})
	}

	function xBox(){

		Swal2.fire({
		  title: 'XBox',
		  text: 'Xbox is a video gaming brand created and owned by Microsoft. The brand consists of five video game consoles, as well as applications (games), streaming service Xbox Cloud Gaming, online services such as the Xbox network and Xbox Game Pass, and the development arm Xbox Game Studios. The brand was first introduced in the United States in November 2001, with the launch of the original Xbox console.',
		  imageUrl: 'https://logos-world.net/wp-content/uploads/2020/11/Xbox-Logo.png',
		  imageWidth: 400,
		  imageHeight: 200,
		  imageAlt: 'Custom image',
		})
	}

	return (
		
		<Container className= "mt-5">
			<Row >
				<Col className= "col-12 col-md-4 mt-3 ">
					<Card className= "cardHighlight ">
					      <Card.Body className='text-bg-primary m-3'>
							
					        <Button className='m-3' variant="light" onClick={playStation}>PlayStation</Button>
					        <Card.Text>
					          PlayStation is a video game brand created and owned by Sony Interactive Entertainment (SIE), a wholly-owned subsidiary of Sony Corporation.
					          The brand was first introduced in 1994 with the release of the PlayStation console, and has since become one of the most successful and popular video game brands in the world.					         
					          PlayStation has a large library of games, including both first-party and third-party titles. Some of the most popular PlayStation games include the God of War series, the Uncharted series, the Gran Turismo series, and the Horizon Zero Dawn series.
					          PlayStation is a major player in the video game industry, and it is one of the most popular gaming platforms in the world. As of March 2023, PlayStation consoles have sold over 581 million units worldwide.
					          PlayStation is known for its innovative technology and its commitment to providing gamers with the best possible gaming experience. The company has been praised for its focus on graphics, gameplay, and immersion.
					          PlayStation is also known for its strong first-party lineup of games. The company has a number of internal studios that develop games exclusively for PlayStation consoles. This gives PlayStation a significant advantage over its competitors, as it allows the company to control the quality and consistency of its games.
					        </Card.Text>
					      </Card.Body>
					    </Card>
				</Col>

				<Col className= "col-12 col-md-4 mt-3">
					<Card className= "cardHighlight">
					      <Card.Body className='text-bg-danger m-3'>
					      	
					        <Button className='m-3' variant="light" onClick={nintendoSwitch}>Nintendo Switch</Button>
					        <Card.Text>
					          Nintendo Switch is a hybrid video game console developed by Nintendo and released worldwide in most regions on March 3, 2017. The console itself is a tablet that can either be docked for home console use or used as a portable device, making it a hybrid console. Its wireless Joy-Con controllers, with standard buttons and directional analog sticks for user input, motion sensing, and tactile feedback, can attach to both sides of the console to support handheld-style play.
					          Nintendo Switch has been a commercial success, selling over 103 million units worldwide as of March 31, 2023. It is the best-selling console of the eighth generation, and the ninth best-selling console of all time.
					          Nintendo Switch has been praised for its versatility, portability, and large library of games. It has also been criticized for its high price point and lack of backwards compatibility with previous Nintendo consoles.
					          Nintendo Switch is a major player in the video game industry, and it is likely to remain popular for years to come.
					        </Card.Text>
					      </Card.Body>
					    </Card>
				</Col>

				<Col className= "col-12 col-md-4 mt-3">
					<Card className= "cardHighlight">
					      <Card.Body className='text-bg-dark m-3'>
					      	
					        <Button className='m-3' variant="light" onClick={xBox}>XBox</Button>
					        <Card.Text>
					          Xbox is a video game brand created and owned by Microsoft. The brand was first introduced in 2001 with the release of the original Xbox console, and has since become one of the most successful and popular video game brands in the world. 
					          Xbox consoles have been released in four generations: Xbox (2001–2005), Xbox 360 (2005–2013), Xbox One (2013–2020), and Xbox Series X/S (2020–present). 					         

					          Xbox has a large library of games, including both first-party and third-party titles. Some of the most popular Xbox games include the Halo series, the Gears of War series, the Forza series, and the Sea of Thieves series.

					          Xbox is a major player in the video game industry, and it is one of the most popular gaming platforms in the world. As of March 2023, Xbox consoles have sold over 165 million units worldwide.

					          Xbox is known for its innovative technology and its commitment to providing gamers with the best possible gaming experience. The company has been praised for its focus on online gaming, social features, and ease of use.

					          Xbox is also known for its strong first-party lineup of games. The company has a number of internal studios that develop games exclusively for Xbox consoles. This gives Xbox a significant advantage over its competitors, as it allows the company to control the quality and consistency of its games.
					        </Card.Text>
					      </Card.Body>
					    </Card>
				</Col>
			</Row>
		</Container>
		
	)
}