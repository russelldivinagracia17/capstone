import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom';



export default function Cartcard(props){

	const {productId, name, quantity, subtotal, purchasedOn} = props.productProp;




	return(
		<Container>
			<Row  className ='col-12'>
				<Col className ="m-3">
					<Card> 
	                      <Card.Body>
	                        <Card.Title>Cart</Card.Title>

	                        <Card.Subtitle>Name:</Card.Subtitle>
	                        <Card.Text>{name}</Card.Text>

	                        <Card.Subtitle>Description:</Card.Subtitle>
	                        <Card.Text>{quantity}</Card.Text>

	                        <Card.Subtitle>Subtotal:</Card.Subtitle>
	                        <Card.Text>Php {subtotal}</Card.Text>

	                        <Card.Subtitle>Date of Purchased</Card.Subtitle>
	                        <Card.Text>{purchasedOn}</Card.Text>
	                        

	                      </Card.Body>
	                </Card>
				</Col>
			</Row>
		</Container>
	)
}