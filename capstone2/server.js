const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js");
const cartRoutes = require("./routes/cartRoutes.js");


const port = 9000;

const app = express();

// Mongoose Connection

mongoose.connect("mongodb+srv://admin:admin@batch288divinagracia.5bsmxzx.mongodb.net/GameBlitz?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;

db.on("error", console.error.bind(console, "Error, Cant connect to the database!"));

db.once("open", () => console.log("You are now connected to the database!"))

// Middlewares

app.use(express.json());

app.use(express.urlencoded({extended: true}));

app.use(cors());

app.use("/users", userRoutes);

app.use("/products", productRoutes);

app.use("/orders", orderRoutes);

app.use("/carts", cartRoutes);

app.listen(port, console.log(`Server is now running at port ${port}!`));