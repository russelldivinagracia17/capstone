const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
	userId: {
		type: String,
		require: [true, "Cart user Id is required!"]
	},
	products: [
		{
			productId: {
				type: String,
				require: [true, "Cart product Id is required!"]
			},
			quantity: {
				type: Number,
				require: [true, "Cart quantity is required!"]
			},
			subtotal: {
				type: Number,
				require: [true, "Cart subtotal is required!"]
			},
			purchasedOn: {
				type: Date,
				default: new Date
			}
		}
	],
	totalAmount: {
		type: Number,
		require: [true, "Cart total amount is required!"]
	}
})

const Carts = new mongoose.model("Cart", cartSchema);

module.exports = Carts;