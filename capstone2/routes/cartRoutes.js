const express = require("express");

const cartControllers = require("../controllers/cartControllers.js");

const auth = require("../auth.js");

const router = express.Router();

// Routes

router.post("/createCart", auth.verify, cartControllers.createCart);

router.post("/addToCart", auth.verify, cartControllers.addToCart);

router.patch("/changeQuantity", auth.verify, cartControllers.updateQuantity);

router.delete("/deleteProduct", auth.verify, cartControllers.deleteProduct)

router.get("/cartProduct", auth.verify, cartControllers.viewCart)

module.exports = router;