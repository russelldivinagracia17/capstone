const express = require("express");

const orderControllers = require("../controllers/orderControllers.js");

const auth = require("../auth.js")

const router = express.Router();

// Routes

router.post("/createOrder", auth.verify, orderControllers.singleOrder);

router.post("/multipleOrders", auth.verify, orderControllers.multipleOrders);

router.get("/allOrders", auth.verify, orderControllers.findAll)


module.exports = router;