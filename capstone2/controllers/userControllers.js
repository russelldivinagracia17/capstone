const Users = require("../models/Users.js");

const Orders = require("../models/Orders.js")

const bcrypt = require("bcrypt")

const auth = require("../auth.js")

module.exports.registerUser = (request, response) => {

	Users.findOne({email: request.body.email})

	.then(result => {

		if(result){

			response.send(false)

		} else {

			let newUser = new Users({
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 10),
				isAdmin: request.body.isAdmin
			})

			newUser.save()
			.then(save => response.send(true))
			.catch(error => response.send(false));
		}

	}).catch(error => response.send(false));


}

module.exports.loginUser = (request, response) => {

	Users .findOne({email: request.body.email})
	.then(result => {

		if(!result){
			return response.send(false)
		} else {

			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				return response.send({
					auth: auth.createAccessToken(result)
				})
			} else {
				return response.send(false)
			}  

		}
	})
	.catch(error => response.send(false));
}


module.exports.userDetails = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const userId = userData.id;

	Users.findOne({_id: userId})
	.then(result => {

		if(result){

			Users.findById(userId)
			.then(result => {

				Orders.find({userId: userId})
				.then(order => {
					response.send(`${result} \n ${order}`)
				})
			})
			.catch(error => response.send(error));

		} else {

			return response.send(`User not Found!`)
		}
	})
}

module.exports.updateUser = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const userId = request.params.userId;

	let updateUser = {
		isAdmin: request.body.isAdmin
	}

	if(userData.isAdmin){

		Users.findByIdAndUpdate(userId, updateUser)
		.then(result => response.send(`Successfully Updated!`))
		.catch(error => response.send(error));


	} else {

		return response.send(`You Don't have access to this Route!`)
	}
}

module.exports.userOrders = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){

		return response.send(false);

	} else {

		Orders.find({userId: userData.id})
		.then(order => response.send(order))
		.catch(error => response.send(false));
	}
}

module.exports.userInfo = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	Users.findOne({_id: userData.id})
	.then(data => response.send(data))

}


